// ==UserScript==
// @name           IGLink Skipper
// @description    IGLink Skipper is designed to automatically skip IGLink pages.
// @homepage       https://gitlab.com/adblocker/iglink-skipper/
// @version        6.0
// @include        https://iglink.pw/*
// @include        https://igtools.net/*
// @include        https://tvyarismalarim.com/*
// @run-at         document-end
// @grant          GM_addStyle
// ==/UserScript==

(function main() {
    if (/igtools\.net/.test (location.hostname) ) {
        pressLink('btn btn-block btn-gradient-warning');
    } else if (/tvyarismalarim\.com/.test (location.hostname) ) {
        exec(removeTimer);
        pressLink('btn btn-lg btn-success');
    } else if (/iglink\.pw/.test (location.hostname) ) {
        exec(removeTimer);
        pressLink('btn btn-lg btn-success');
    }
})();

function exec(fn) {
    var script = document.createElement('script');
    script.setAttribute("type", "text/javascript");
    script.textContent = '(' + fn + ')();';
    document.body.appendChild(script);
    document.body.removeChild(script);
}

function removeTimer() {
	$("div.after").hide();
	$(function(){
		var saniye = 0;
		var sayacYeri = $("div.sayac span");
		$.sayimiBaslat = function(){
			if(saniye > 1){
				saniye--;
				sayacYeri.text(saniye);
			} else {
				$("div.sayac").hide();
				$("div.after").show();
				$("#createLink").show();
			}
		}
		sayacYeri.text(saniye);
		setInterval("$.sayimiBaslat()", 1);
	});
};

function pressLink(className) {
    var targetURL  = document.getElementsByClassName(className)[0].href;
    if (targetURL.length > 1)
        window.location.href = targetURL;
}
